import matplotlib.pyplot as plt
import math

l=""
def loadtovariables(l, delimeter):
	f1=open(l, "r")
	var1=[]
	var2=[[],[]]
	for x in f1.readlines():
		var1.append(str(x).rstrip("\n").rsplit(delimeter))
	return var1

model=loadtovariables("model_smg.dat", "\t")
ncounts=loadtovariables("ncounts_850.dat", ":")
modelconverted=model
ncounts2=ncounts

xi=[]
h1=0
h2=0
#Coverting model to the same units as ncounts
for x in range(len(model)):
		#xi.append(float(model[x][0]))
		if x==0:
			h1=float(model[x][0])
			h2=float(model[x+1][0])-float(model[x][0])
			modelconverted[x][1]=h1*float(model[x+1][1])/(h2*(h1+h2)) - float(model[x][1])*(h1-h2)/(h1*h2) - float(model[x][1])*h2/(h1*(h1+h2))
		elif x>1:
			if x!=(len(model)-1):
				h1=float(model[x][0])-float(model[x-1][0])
				h2=float(model[x][0])-float(model[x][0])
				modelconverted[x][1]=h1*float(model[x+1][1])/(h2*(h1+h2)+.000001) - float(model[x][1])*(h1-h2)/(h1*h2+.0000001) - float(model[x-1][1])*h2/(h1*(h1+h2)+.0000001)
	
		elif x==len(model):
			h1=float(model[x][0])-float(model[x-1][0])
			h2=float(model[x][0])
			modelconverted[x][1]=h1*float(model[x][1])/(h2*(h1+h2)) - float(model[x][1])*(h1-h2)/(h1*h2) - float(model[x][1])*h2/(h1*(h1+h2))
		modelconverted[x][0]=math.log(abs(float(modelconverted[x][0])) +.000001, 10)
		modelconverted[x][1]=math.log(abs(float(modelconverted[x][1])) +.000001, 10)
#print(ncounts)

plt.plot(modelconverted, "ro")
plt.plot(ncounts, "bo")
plt.xlabel("log10(L)")
plt.ylabel('log10(dN/dL)')
plt.title("Luminosity vs. Differential of Luminosity")
#plt.axis([0, 100, -10, 10])
plt.show()


