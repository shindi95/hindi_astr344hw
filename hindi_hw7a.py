import random
import math

print random.random()

def myrando():
	return random.random()

total=0
incircle=0
randocoord=[0,0]
iterations=10000

for i in range(iterations):
	randocoord=[myrando(), myrando()]
	total+=1
	if randocoord[1] < math.sqrt(1-randocoord[0]**2):
		incircle+=1

print "Pi= ", 4*(float(incircle)/float(total))