import math
import matplotlib.pyplot as plt
import numpy as np

#This is function we are integrating
def f(z):
	return (3.0*(10.0**3))/math.sqrt( (.3*(1.+z)**3. + 0*(1.+z)**2.) + .7)

#The integration distance
def integrator(start, end, step):
	total=0
	datarange=[start]; loop=start

	#We need to create the datarangevariable first. This computes the X values where for the trapazoid.
	while loop < end:
		datarange.append(step+loop)
		loop+=step
		if loop > end:
			break
	#This is implementing the trapazoid rule
	for i in datarange:
		total+=f(i)*step+.5*(f(i+step)-f(i))*step
		#print( f(i)*step+.5*(f(i+step)-f(i))*step )
		#print(f(i))
		#print(range(start, end, step))
	return total

#this is Da(z)
def D(z):
	#if switch==0:	
	return integrator(0, z, .00001)/(1.+z)
	#if switch==1:
	#	return np.trapz(np.trapz(0, z, .00001)/(1.+z))
#This section plots the points
points=[]
pointsTrapz=[]
for i in range(6):
	points.append(D(i))
	#pointsTrapz.append(D(i,1))
plt.plot(points)
#plt.plot(pointsTrapz)
plt.show()
