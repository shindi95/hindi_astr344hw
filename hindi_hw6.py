import matplotlib.pyplot as plt
import math
from datetime import datetime

def f(z, t):
	if t==0: return math.sin(z)
	if t==1: return (z**3)-z-2.0
	if t==2: return -6.0+z+z**2.0
	if t==3: return ((2.0*(-27.0)*(10/870.0)**3)/(100.0))/(math.e**( ((-27.0)*(10/870.0))/((-16.0)*z) ) -1.0000001)
	#Note, the above line is my equation for blackbody temperature. I have no idea why it's not working. It must be the physics.

def intersect(x,y,m):
	b=y-m*x
	return -b/(m+.0000001)

def NR(s):
	xi=1.0
	intersection=0
	newcoordinate=[0,0]
	for i in range(150):
		#(f(xi+.0002, s)-f(xi, s)) / (.0002)
		newcoordinate=[xi, f(xi,s)]
		xi=intersect( newcoordinate[0], newcoordinate[1], (f(xi+.0002, s)-f(xi, s)) / (.0002  ))	
	return newcoordinate[0]

def findroot(x, s):
    n=2
    addorsub=[1,-1]
    error=0.01
    signTracker=[-1.0]
    while math.fabs( f(x[n%2], s) ) > error and math.fabs( f(x[(n+1)%2], s) ) > error:   
        if signTracker[len(signTracker)-1] != signTracker[len(signTracker)-2]:
            n+=1
        elif signTracker[len(signTracker)-1] == signTracker[len(signTracker)-2]:
            x[n%2]=.001*addorsub[n%2]+ x[n%2]

        if x[0] > x[1]: n+=1
    
        signTracker.append(math.copysign(1, f(x[n%2], s)/f(x[(n+1)%2],s)))
    return x[0]

times=[datetime.now(),0,0]

print "\nNewton Rapshon Method:"
print "Function 1: Root at x=", NR(0)
print "Function 2: Root at x=", NR(1)
print "Function 3: Root at x=", NR(2)

times[1]=datetime.now()

print "\nBisection Method:"
print "Function 1: Root at x=", findroot([2,4], 0)
print "Function 2: Root at x=", findroot([1,2], 1)
print "Function 3: Root at x=", findroot([0,5], 2)

times[2]=datetime.now()

print "\nThe Newton Rapshon method is " + str(times[2]-times[1]-(times[1]-times[0])) + " faster"

print "\nThe correct temperature is: ", NR(3)