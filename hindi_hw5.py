#################################################################
#The general logic: x1, and x2 are chosen
#as the starting coordinates and x1 is moved
#closer and closer to x2 until f(x1) changes signs.
#At that point n+=1 which means the "active" x, ie the one
#that moves closer to the midpoint is changed to x2. n%2 controls
#which is the active x. This happens until f(x1) or f(x2) is 
#within error of 0.
#

import math

def f(z, t):
    if t==0: return math.sin(z)
    if t==1: return (z**3)-z-2.0
    if t==2: return -6.0+z+z**2.0

def findroot(x, s):
    n=6
    addorsub=[1,-1]
    error=0.01
    signTracker=[-1.0]
    while math.fabs( f(x[n%2], s) ) > error and math.fabs( f(x[(n+1)%2], s) ) > error:   
        if signTracker[len(signTracker)-1] != signTracker[len(signTracker)-2]:
            n+=1
        elif signTracker[len(signTracker)-1] == signTracker[len(signTracker)-2]:
            x[n%2]=.001*addorsub[n%2]+ x[n%2]

        if x[0] > x[1]: n+=1
    
        signTracker.append(math.copysign(1, f(x[n%2], s)/f(x[(n+1)%2],s)))
    return x[0]

print "Function 1: Root at x=", findroot([2,4], 0)
print "Function 2: Root at x=", findroot([1,2], 1)
print "Function 3: Root at x=", findroot([0,5], 2)
